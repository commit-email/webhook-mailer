# Copyright (C) 2010-2018  Kouhei Sutou <kou@clear-code.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

class AppTest < Test::Unit::TestCase
  include Helper

  class << self
    include Helper::Fixture

    def startup
      @rroonga_git_dir = fixture_path("rroonga.git")
      system("git", "clone", "--mirror", "-q",
             "https://github.com/ranguba/rroonga.git", @rroonga_git_dir)
    end

    def shutdown
      FileUtils.rm_rf(@rroonga_git_dir)
    end
  end

  def setup
    setup_tmp_dir
    setup_app(app_options)
  end

  def teardown
    teardown_app
    teardown_tmp_dir
  end

  def app_options
    @commit_mailer_inputs = []
    {
      "enabled" => false,
      "base_dir" => @tmp_dir,
      "repository_class" => LocalRepository,
      "commit_mailer_stub" => lambda {|*input| @commit_mailer_inputs << input},
      "to" => "null@example.com",
      "owners" => {
        "ranguba" => {
          "enabled" => true,
        },
      },
    }
  end

  def test_get
    visit "/"
    assert_response(:method_not_allowed, "must POST")
  end

  def test_post_ping
    payload = {
      "zen" => "Speak like a human.",
      "hook_id" => 2043443,
    }
    env = {
      "HTTP_X_GITHUB_EVENT" => "ping",
    }
    post_payload(payload, env)
    assert_response(:ok, "")
    assert_equal("", body)
  end

  def test_post_without_parameters
    page.driver.post("/")
    assert_response(:bad_request, "payload is missing")
  end

  def test_post_with_empty_payload
    page.driver.post("/", :payload => "")
    error_message = nil
    begin
      JSON.parse("")
    rescue
      error_message = $!.message
    end
    assert_response(:bad_request,
                    "invalid JSON format: <#{error_message}>")
  end

  class GitHubTest < self
    class << self
      def startup
      end

      def shutdown
      end
    end

    def test_post_with_non_target_repository
      owner_name = "devil"
      repository_name = "evil-repository"
      post_payload(:repository => {
                     :name => repository_name,
                     :html_url => "https://github.com/super-devil/evil-repository",
                     :url => "https://github.com/super-devil/evil-repository",
                     :clone_url => "https://github.com/super-devil/evil-repository.git",
                     :owner => {
                       :name => owner_name,
                     },
                   })
      assert_response(:accepted,
                      "ignore disabled repository: " +
                        "<#{owner_name.inspect}>:<#{repository_name.inspect}>")
    end

    def test_post_without_owner
      repository = {
        "html_url" => "https://github.com/ranguba/rroonga",
        "url" => "https://github.com/ranguba/rroonga",
        "clone_url" => "https://github.com/ranguba/rroonga.git",

        "name" => "rroonga",
      }
      payload = {
        "repository" => repository,
      }
      error = unexpected_payload_error("repository owner is missing", payload)
      assert_raise(error) do
        post_payload(payload)
      end
    end

    def test_post_without_owner_name
      repository = {
        "html_url" => "https://github.com/ranguba/rroonga",
        "url" => "https://github.com/ranguba/rroonga",
        "clone_url" => "https://github.com/ranguba/rroonga.git",
        "name" => "rroonga",
        "owner" => {},
      }
      payload = {
        "repository" => repository,
      }
      error = unexpected_payload_error("repository owner is missing", payload)
      assert_raise(error) do
        post_payload(payload)
      end
    end

    def test_post_without_before
      payload = {
        "repository" => {
          "html_url" => "https://github.com/ranguba/rroonga",
          "url" => "https://github.com/ranguba/rroonga",
          "clone_url" => "https://github.com/ranguba/rroonga.git",
          "name" => "rroonga",
          "owner" => {
            "name" => "ranguba",
          },
        }
      }
      error = unexpected_payload_error("before commit ID is missing", payload)
      assert_raise(error) do
        post_payload(payload)
      end
    end

    def test_post_without_after
      payload = {
        "before" => "0f2be32a3671360a323f1dee64c757bc9fc44998",
        "repository" => {
          "html_url" => "https://github.com/ranguba/rroonga",
          "url" => "https://github.com/ranguba/rroonga",
          "clone_url" => "https://github.com/ranguba/rroonga.git",
          "name" => "rroonga",
          "owner" => {
            "name" => "ranguba",
          },
        },
      }
      error = unexpected_payload_error("after commit ID is missing", payload)
      assert_raise(error) do
        post_payload(payload)
      end
    end

    def test_post_without_reference
      payload = {
        "before" => "0f2be32a3671360a323f1dee64c757bc9fc44998",
        "after" => "c7bf92799225d67788be7c42ea4f504a47708390",
        "repository" => {
          "html_url" => "https://github.com/ranguba/rroonga",
          "url" => "https://github.com/ranguba/rroonga",
          "clone_url" => "https://github.com/ranguba/rroonga.git",
          "name" => "rroonga",
          "owner" => {
            "name" => "ranguba",
          },
        },
      }
      error = unexpected_payload_error("reference is missing", payload)
      assert_raise(error) do
        post_payload(payload)
      end
    end

    def test_post
      repository_mirror_path = mirror_path("github.com", "ranguba", "rroonga")
      assert_false(File.exist?(repository_mirror_path))
      before = "0f2be32a3671360a323f1dee64c757bc9fc44998"
      after = "c7bf92799225d67788be7c42ea4f504a47708390"
      reference = "refs/heads/master"
      post_payload(:repository => {
                     :html_url => "https://github.com/ranguba/rroonga",
                     :url => "https://github.com/ranguba/rroonga",
                     :clone_url => "https://github.com/ranguba/rroonga.git",
                     :name => "rroonga",
                     :owner => {
                       :name => "ranguba",
                     },
                   },
                   :before => before,
                   :after => after,
                   :ref => reference)
      assert_response(:ok, "")
      assert_true(File.exist?(repository_mirror_path))
      assert_equal([
                     [
                       [
                         "--repository", repository_mirror_path,
                         "--max-size", "1M",
                         "--repository-browser", "github",
                         "--github-user", "ranguba",
                         "--github-repository", "rroonga",
                         "--name", "ranguba/rroonga",
                         "null@example.com",
                       ],
                       before,
                       after,
                       reference,
                     ],
                   ],
                   @commit_mailer_inputs)
    end

    sub_test_case "owner" do
      def app_options
        super.merge("owners" => {
                      "ranguba" => {
                        "enabled" => true,
                        "to" => "ranguba-commit@example.org",
                        "from" => "ranguba+commit@example.org",
                        "sender" => "null@example.org",
                      }
                    })
      end

      def test_post
        repository_mirror_path = mirror_path("github.com", "ranguba", "rroonga")
        assert do
          not File.exist?(repository_mirror_path)
        end
        before = "0f2be32a3671360a323f1dee64c757bc9fc44998"
        after = "c7bf92799225d67788be7c42ea4f504a47708390"
        reference = "refs/heads/master"
        post_payload(:repository => {
                       :html_url => "https://github.com/ranguba/rroonga",
                       :url => "https://github.com/ranguba/rroonga",
                       :clone_url => "https://github.com/ranguba/rroonga.git",
                       :name => "rroonga",
                       :owner => {
                         :name => "ranguba",
                       },
                     },
                     :before => before,
                     :after => after,
                     :ref => reference)
        assert_response(:ok, "")
        assert do
          File.exist?(repository_mirror_path)
        end
        assert_equal([
                       [
                         [
                           "--repository", repository_mirror_path,
                           "--max-size", "1M",
                           "--repository-browser", "github",
                           "--github-user", "ranguba",
                           "--github-repository", "rroonga",
                           "--name", "ranguba/rroonga",
                           "--from", "ranguba+commit@example.org",
                           "--sender", "null@example.org",
                           "ranguba-commit@example.org",
                         ],
                         before,
                         after,
                         reference
                       ],
                     ],
                     @commit_mailer_inputs)
      end
    end

    sub_test_case "repository" do
      def app_options
        super.merge("owners" => {
                      "ranguba" => {
                        "to" => "ranguba-commit@example.org",
                        "from" => "ranguba+commit@example.org",
                        "sender" => "null@example.org",
                        "repositories" => {
                          "rroonga" => {
                            "enabled" => true,
                            "to" => "ranguba-commit@example.net",
                            "from" => "ranguba+commit@example.net",
                            "sender" => "null@example.net",
                          }
                        }
                      }
                    })
      end

      def test_post
        repository_mirror_path = mirror_path("github.com", "ranguba", "rroonga")
        assert do
          not File.exist?(repository_mirror_path)
        end
        before = "0f2be32a3671360a323f1dee64c757bc9fc44998"
        after = "c7bf92799225d67788be7c42ea4f504a47708390"
        reference = "refs/heads/master"
        post_payload(:repository => {
                       :html_url => "https://github.com/ranguba/rroonga",
                       :url => "https://github.com/ranguba/rroonga",
                       :clone_url => "https://github.com/ranguba/rroonga.git",
                       :name => "rroonga",
                       :owner => {
                         :name => "ranguba",
                       },
                     },
                     :before => before,
                     :after => after,
                     :ref => reference)
        assert_response(:ok, "")
        assert do
          File.exist?(repository_mirror_path)
        end
        assert_equal([
                       [
                         [
                           "--repository", repository_mirror_path,
                           "--max-size", "1M",
                           "--repository-browser", "github",
                           "--github-user", "ranguba",
                           "--github-repository", "rroonga",
                           "--name", "ranguba/rroonga",
                           "--from", "ranguba+commit@example.net",
                           "--sender", "null@example.net",
                           "ranguba-commit@example.net",
                         ],
                         before,
                         after,
                         reference,
                       ],
                     ],
                     @commit_mailer_inputs)
      end
    end

    def test_gollum
      repository_mirror_path =
        mirror_path("github.com", "ranguba", "rroonga.wiki")
      assert_false(File.exist?(repository_mirror_path))
      before = "83841cd1576e28d85aa5ec312fd3804d1352e5ab^"
      after = "83841cd1576e28d85aa5ec312fd3804d1352e5ab"
      reference = "refs/heads/master"
      payload = {
        "repository" => {
          "html_url" => "https://github.com/ranguba/rroonga",
          "url" => "https://github.com/ranguba/rroonga",
          "clone_url" => "https://github.com/ranguba/rroonga.git",
          "name" => "rroonga",
          "owner" => {
            "login" => "ranguba",
          },
        },
        "pages" => [
          {
            "sha" => "83841cd1576e28d85aa5ec312fd3804d1352e5ab",
          },
        ],
      }
      env = {
        "HTTP_X_GITHUB_EVENT" => "gollum",
      }
      post_payload(payload, env)
      assert_response(:ok, "")
      assert do
        File.exist?(repository_mirror_path)
      end
      assert_equal([
                     [
                       [
                         "--repository", repository_mirror_path,
                         "--max-size", "1M",
                         "--repository-browser", "github-wiki",
                         "--github-user", "ranguba",
                         "--github-repository", "rroonga",
                         "--name", "ranguba/rroonga.wiki",
                         "null@example.com",
                       ],
                       before,
                       after,
                       reference,
                     ],
                   ],
                   @commit_mailer_inputs)
    end
  end

  private
  def default_env
    {
      "HTTP_X_GITHUB_EVENT" => "push",
    }
  end

  def mirror_path(*components)
    File.join(@tmp_dir, "mirrors", *components)
  end
end
