# Copyright (C) 2010-2018  Kouhei Sutou <kou@clear-code.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

require "yaml"
require "rack/utils"

require "webhook-mailer"

module Helper
  include Capybara::DSL

  private
  def unexpected_payload_error(message, raw_payload)
    payload = WebhookMailer::Payload.new(raw_payload)
    WebhookMailer::UnexpectedPayloadError.new(message, payload)
  end

  def assert_response(expected_status_code, expected_message)
    assert_equal([resolve_status(expected_status_code), expected_message],
                 [resolve_status(status_code), page.body])
  end

  def resolve_status(status)
    code = Rack::Utils.status_code(status)
    pp [status] if code.zero?
    [code, Rack::Utils::HTTP_STATUS_CODES[code]]
  end

  def default_env
    {}
  end

  def post_payload(payload, env={})
    env = default_env.merge(env)
    page.driver.post("/",
                     {:payload => JSON.generate(payload)},
                     env)
    wait_response
  end

  def setup_app(app_options)
    @notifier = Nofifier.new
    @processing = false
    options = app_options.dup
    options["processing"] = lambda do
      @processing = true
    end
    options["processed"] = lambda do
      @processing = false
      @notifier.notify
    end
    Capybara.app = WebhookMailer::App.new(options)
  end

  def teardown_app
    @notifier.close
  end

  def wait_response
    @notifier.wait if @processing
  end

  def setup_tmp_dir
    @tmp_dir = File.join(__dir__, "tmp")
    FileUtils.mkdir_p(@tmp_dir)
  end

  def teardown_tmp_dir
    FileUtils.rm_rf(@tmp_dir)
  end

  module Fixture
    def fixture_path(*components)
      File.join(__dir__, "fixtures", *components)
    end
  end
  include Fixture

  class LocalRepository < WebhookMailer::Repository
    extend Fixture

    private
    def repository_uri
      fixture_path("#{@name}.git")
    end
  end

  class Nofifier
    def initialize
      @in, @out = IO.pipe
    end

    def notify
      @out.print("X")
      @out.flush
    end

    def wait
      IO.select([@in])
      @in.read(1)
    end

    def close
      @in.close
      @out.close
    end
  end
end
