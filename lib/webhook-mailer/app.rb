# Copyright (C) 2010-2019  Sutou Kouhei <kou@clear-code.com>
# Copyright (C) 2015  Kenji Okimoto <okimoto@clear-code.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

require "webhook-mailer/ci-mailer"
require "webhook-mailer/error"
require "webhook-mailer/logger"
require "webhook-mailer/options"
require "webhook-mailer/path-resolver"
require "webhook-mailer/payload"
require "webhook-mailer/repository"
require "webhook-mailer/response"

module WebhookMailer
  class App
    def initialize(options={})
      @options = Options.new(options)
    end

    def call(env)
      request = Rack::Request.new(env)
      response = Response.new
      process(request, response) or response.finish
    end

    private
    def process(request, response)
      unless request.post?
        response.set(:method_not_allowed, "must POST")
        return nil
      end

      Logger.log("request.log", request)
      payload = parse_payload(request, response)
      return nil if payload.nil?
      Logger.log("payload.log", payload)
      process_payload(request, response, payload)
    end

    def parse_payload(request, response)
      mime_type = request.content_type.split(/\s*;\s*/).first
      if mime_type == "application/json"
        payload = request.body.read
      else
        payload = request.params["payload"]
      end
      if payload.nil?
        response.set(:bad_request, "payload is missing")
        return nil
      end

      begin
        JSON.parse(payload)
      rescue JSON::ParserError
        response.set(:bad_request, "invalid JSON format: <#{$!.message}>")
        nil
      end
    end

    def process_payload(request, response, raw_payload)
      metadata = {
        "x-github-event" => github_event(request),
        "x-gitlab-event" => gitlab_event(request),
      }
      payload = Payload.new(raw_payload, metadata)
      case payload.event_name
      when "ping"
        # Do nothing
        nil
      when "push", "gollum", "wiki_page"
        create_repository(request, response, payload) do |repository|
          process_commit(request, response, payload, repository)
        end
      when "ci"
        create_repository(request, response, payload) do |repository|
          process_ci(request, response, payload, repository)
        end
      else
        response.set(:bad_request,
                     "Unsupported event: <#{payload.event_name}>")
        nil
      end
    end

    def github_event(request)
      request.env["HTTP_X_GITHUB_EVENT"]
    end

    def gitlab_event(request)
      request.env["HTTP_X_GITLAB_EVENT"]
    end

    def create_repository(request, response, payload)
      repository_class = @options[:repository_class] || Repository
      repository = repository_class.new(payload, @options)
      unless repository.enabled?
        response.set(:accepted,
                     "ignore disabled repository: " +
                     "<#{repository.owner.inspect}>:" +
                     "<#{repository.name.inspect}>")
        return nil
      end
      yield(repository)
    end

    def process_commit(request, response, payload, repository)
      change = payload.change
      reference = change[-1]
      unless repository.target_reference?(reference)
        response.set(:accepted,
                     "not target reference: " +
                     "<#{repository.owner.inspect}>:" +
                     "<#{repository.name.inspect}>:" +
                     "<#{reference.inspect}>")
        return nil
      end
      response.finish do
        run_async do
          repository.process_commit(*change)
        end
      end
    end

    def process_ci(request, response, payload, repository)
      return nil unless payload.finished?
      response.finish do
        run_async do
          repository.process_ci
        end
      end
    end

    def run_async
      processing_callback = @options[:processing]
      processing_callback.call if processing_callback
      Thread.new do
        begin
          yield
        ensure
          processed_callback = @options[:processed]
          processed_callback.call if processed_callback
        end
      end
    end
  end
end
