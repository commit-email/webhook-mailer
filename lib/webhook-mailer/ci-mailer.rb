# Copyright (C) 2018  Kouhei Sutou <kou@clear-code.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

module WebhookMailer
  class CIMailer
    include ERB::Util

    def initialize(previous_state, payload, options)
      @previous_state = previous_state
      @payload = payload
      @options = options
      @boundary = @options[:boundary] || generate_boundary
    end

    def send_email
      server = "localhost"
      port = Net::SMTP.default_port
      from = @options[:sender] || @options[:from]
      raw_from = GitCommitMailer.extract_email_address(from)
      raw_to = Array(@options[:to]).collect do |to|
        GitCommitMailer.extract_email_address(to)
      end
      stub = @options[:ci_mailer_stub]
      if stub.respond_to?(:call)
        stub.call(server, port, raw_from, raw_to, mail)
      else
        GitCommitMailer.send_mail(server, port, raw_from, raw_to, mail)
      end
    end

    private
    def generate_boundary
      random_integer = Time.now.to_i * 1000 + rand(1000)
      Digest::SHA1.hexdigest(random_integer.to_s)
    end

    def header
      <<-HEADER
X-Mailer: WebhookMailer/#{VERSION}
MIME-Version: 1.0
Content-Type: multipart/alternative;
 boundary=#{@boundary}
From: #{@payload.service_name} <#{@options[:from] || @options[:sender]}>
Sender: #{@options[:sender] || @options[:from]}
To: #{Array(@options[:to]).join(', ')}
Subject: #{subject}
Date: #{@payload.finished_at.rfc2822}
      HEADER
    end

    def subject
      repository = "#{@payload.repository_owner}/#{@payload.repository_name}"
      "#{repository} #{@payload.status_label} " +
        "[#{@payload.branch}] #{@payload.short_commit_id}"
    end

    def previous_status_label
      status_label = @previous_state["status_label"]
      status_label ||= @previous_state["status"]
      status_label || "None"
    end

    def body
      <<-BODY
--#{@boundary}
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit

#{body_text}
--#{@boundary}
Content-Type: text/html; charset=utf-8
Content-Transfer-Encoding: 8bit

#{body_html}
--#{@boundary}--
      BODY
    end

    def each_build(&block)
      allow_failure_builds = []
      @payload.builds.each do |build|
        if build["allow_failure"]
          allow_failure_builds << build
        else
          yield(build)
        end
      end
      unless allow_failure_builds.empty?
        allow_failure_builds.each(&block)
      end
    end

    def body_text
      text = +""
      text << "#{@payload.label}: "
      text << "#{previous_status_label} -> #{@payload.status_label}\n"
      text << "  #{@payload.web_url}\n"
      text << "\n"
      text << "Builds:\n"
      first_allow_failure = true
      each_build do |build|
        if build["allow_failure"] and first_allow_failure
          first_allow_failure = false
          text << "\n"
          text << "Allow failure builds:\n"
        end
        text << body_text_build(build)
      end
      text << "\n"
      text << "Commit author:\n"
      text << "  #{@payload.commit_author}\n"
      text << "Commit date:\n"
      text << "  #{@payload.commit_timestamp.iso8601}\n"
      text << "Commit ID:\n"
      text << "  #{@payload.commit_id}\n"
      text << "Commit changes:\n"
      text << "  #{@payload.compare_url}\n"
      text << "Commit message:\n"
      @payload.commit_message.each_line do |line|
        text << "  #{line.chomp}\n"
      end
      text
    end

    def body_text_build(build)
      name = build["name"]
      status = build["status_label"]
      elapsed_time = format_elapsed_time(build["elapsed_time"])
      url = build["web_url"]
      text = +""
      text << "  "
      text << "#{name}: " unless @payload.appveyor?
      text << "#{status}: #{elapsed_time}:\n"
      text << "    #{url}\n"
      build["conditions"].each do |condition_label, value|
        case value
        when Array
          text << "    #{condition_label}:\n"
          value.each do |v|
            text << "      #{v}\n"
          end
        else
          text << "    #{condition_label}: #{value}\n"
        end
      end
      text
    end

    def body_html
      <<-HTML
<!DOCTYPE html>
<html>
  <head>
  </head>
  <body>
    <h1>
      #{h(@payload.label)}:
      #{h(previous_status_label)} &rarr;
      <a href="#{h(@payload.web_url)}">
        #{status_tag(@payload.status, h(@payload.status_label))}
      </a>
    </h1>
#{body_html_builds.chomp}
    <h2>Commit</h2>
#{body_html_commit.chomp}
  </body>
</html>
      HTML
    end

    def body_html_builds
      html = +"    <h2>Builds</h2>\n"
      html << body_html_builds_header
      first_allow_failure = true
      each_build do |build|
        if build["allow_failure"] and first_allow_failure
          first_allow_failure = false
          html << body_html_builds_footer
          html << "    <h2>Allow failure builds</h2>\n"
          html << body_html_builds_header
        end
        html << body_html_build(build)
      end
      html << body_html_builds_footer
    end

    def body_html_build_labels
      labels = ["Status", "Elapsed"]
      labels << "Name" unless @payload.appveyor?
      condition_labels = []
      @payload.builds.each do |build|
        condition_labels.concat(build["conditions"].keys)
      end
      labels.concat(condition_labels.uniq.sort)
      labels
    end

    def body_html_builds_header
      header = <<-HEADER
    <table>
      <thead>
        <tr>
     HEADER
      body_html_build_labels.each do |label|
        header << "          <th>#{h(label)}</th>\n"
      end
     header << <<-HEADER
        </tr>
      </thead>
      <tbody>
      HEADER
      header
    end

    def body_html_builds_footer
      <<-FOOTER
      </tbody>
    </table>
      FOOTER
    end

    def body_html_build_columns(build)
      conditions = build["conditions"]
      body_html_build_labels.collect do |label|
        case label
        when "Status"
          status = build["status"]
          status_label = build["status_label"]
          tag("a",
              {"href" => build["web_url"]},
              status_tag(status, h(status_label)))
        when "Elapsed"
          h(format_elapsed_time(build["elapsed_time"]))
        when "Name"
          h(build["name"])
        else
          column = conditions[label]
          case column
          when Array
            column.collect do |value|
              h(value)
            end
          else
            h(column)
          end
        end
      end
    end

    def body_html_build(build)
      html = +""
      html << "        <tr>\n"
      body_html_build_columns(build).each do |column|
        case column
        when Array
          html << "          <td>\n"
          html << "            <ul>\n"
          column.each do |value|
            html << "              <li>#{value}</li>\n"
          end
          html << "            </ul>\n"
          html << "          </td>\n"
        else
          html << "          <td>#{column}</td>\n"
        end
      end
      html << "        </tr>\n"
      html
    end

    def body_html_commit
      <<-HTML
    #{dl_start}
      #{dt("Author")}
      #{dd(h(@payload.commit_author))}
      #{dt("Date")}
      #{dd(h(@payload.commit_timestamp.iso8601))}
      #{dt("New revision")}
      #{dd(commit_id_html)}
      #{dt("Message")}
      #{dd(pre(h(@payload.commit_message)))}
    </dl>
      HTML
    end

    def format_elapsed_time(time)
      if time < 60
        "#{time.floor}s"
      elsif time < (60 * 60)
        "#{(time / 60).floor}m#{(time % 60).floor}s"
      else
        "#{(time / 60 / 60).floor}h#{(time / 60 % 60).floor}m#{(time % 60).floor}s"
      end
    end

    def status_tag(status, content)
      case status.downcase
      when "success", "passed"
        background_color = "#a6f3a6"
      when "failed"
        background_color = "#f8cbcb"
      else
        background_color = "#ffffff"
      end
      tag("span",
          {
            "style" => {
              "background-color" => background_color,
              "color"            => "#000000",
            }
          },
          content)
    end

    def commit_id_html
      tag("a",
          {"href" => @payload.compare_url},
          h(@payload.commit_id))
    end

    def tag_start(name, attributes)
      start_tag = +"<#{name}"
      unless attributes.empty?
        sorted_attributes = attributes.sort_by do |key, value|
          key
        end
        formatted_attributes = sorted_attributes.collect do |key, value|
          if value.is_a?(Hash)
            sorted_value = value.sort_by do |value_key, value_value|
              value_key
            end
            value = sorted_value.collect do |value_key, value_value|
              "#{value_key}: #{value_value}"
            end
          end
          if value.is_a?(Array)
            value = value.sort.join("; ")
          end
          "#{h(key)}=\"#{h(value)}\""
        end
        formatted_attributes = formatted_attributes.join(" ")
        start_tag << " #{formatted_attributes}"
      end
      start_tag << ">"
      start_tag
    end

    def tag(name, attributes={}, content=nil, &block)
      block_used = false
      if content.nil? and block_given?
        @indent_level += 1
        if block.arity == 1
          content = []
          yield(content)
        else
          content = yield
        end
        @indent_level -= 1
        block_used = true
      end
      content ||= ""
      if content.is_a?(Array)
        if block_used
          separator = "\n"
        else
          separator = ""
        end
        content = content.join(separator)
      end

      formatted_tag = +""
      formatted_tag << "  " * @indent_level if block_used
      formatted_tag << tag_start(name, attributes)
      formatted_tag << "\n" if block_used
      formatted_tag << content
      formatted_tag << "\n" + ("  " * @indent_level) if block_used
      formatted_tag << "</#{name}>"
      formatted_tag
    end

    def dl_start
      tag_start("dl",
                "style" => {
                  "margin-left" => "2em",
                  "line-height" => "1.5",
                })
    end

    def dt_margin
      8
    end

    def dt(content)
      tag("dt",
          {
            "style" => {
              "clear"       => "both",
              "float"       => "left",
              "width"       => "#{dt_margin}em",
              "font-weight" => "bold",
            },
          },
          content)
    end

    def dd_start
      tag_start("dd",
                "style" => {
                  "margin-left" => "#{dt_margin + 0.5}em",
                })
    end

    def dd(content)
      "#{dd_start}#{content}</dd>"
    end

    def border_styles
      {
        "border" => "1px solid #aaa",
      }
    end

    def pre(content, styles={})
      font_families = [
        "Consolas", "Menlo", "\"Liberation Mono\"",
        "Courier", "monospace"
      ]
      pre_styles = {
        "font-family" => font_families.join(", "),
        "line-height" => "1.2",
        "padding"     => "0.5em",
        "width"       => "auto",
      }
      pre_styles = pre_styles.merge(border_styles)
      tag("pre", {"style" => pre_styles.merge(styles)}, content)
    end

    def mail
      "#{header}\n#{body}".gsub(/\n/, "\r\n")
    end
  end
end
