# Webhook mailer

This is a Rack based Web application that sends commit details, CI result and so on by E-mail via Webhook.

Here are supported services:

  * GitLab
  * GitHub
  * Travis CI
  * AppVeyor

## Set up

Prepare the following files.

/home/webhook-mailer/webhook-mailer/Gemfile:
```ruby
source "https://rubygems.org"
gem "webhook-mailer"
gem "git-commit-mailer"
# gem "unicorn"   # Enable this line if you use Unicorn.
# gem "passenger" # Enable this line if you use latest Passenger.
```

/home/webhook-mailer/webhook-mailer/config.ru:
```ruby
require "yaml"
require "pathname"
require "webhook-mailer"

use Rack::CommonLogger
use Rack::Runtime
use Rack::ContentLength

base_dir = Pathname(__FILE__).dirname
config_file = base_dir + "config.yaml"

options = YAML.load_file(config_file.to_s)

run WebhookMailer::App.new(options)
```

/home/webhook-mailer/webhook-mailer/config.yaml:
```yaml
mirrors_directory: /path/to/mirrors
git_commit_mailer: bin/git-commit-mailer
to: receiver@example.com
sender: sender@example.com
add_html: true
owners:
  groonga:
    to: groonga@ml.commit-email.info
```

See also: https://github.com/kou/commit-email.info/tree/master/ansible/files/home/mailer

### Apache + Passenger

On Debian GNU/Linux stretch.

See also [Phusion Passenger users guide, Apache version](https://www.phusionpassenger.com/documentation/Users%20guide%20Apache.htm)l.

Install Passenger or write `gem "passenger"` in your Gemfile.

```console
$ sudo apt-get install -y ruby-passenger
```

Install gems.

```console
$ sudo -u webhook-mailer -H bundle install --binstubs --path vendor/bundle
```

Prepare following files.

/etc/apache2/mods-available.conf:
```apache
PassengerRoot /path/to/passenger-x.x.x
PassengerRuby /path/to/ruby

PassengerMaxRequests 100
```

/etc/apache2/mods-available.load:
```apache
LoadModule passenger_module /path/to/mod_passenger.so
```

/etc/apache2/sites-available/github-web-hooks-receiver:
```apache
<VirtualHost *:80>
  ServerName webhook-mailer.example.com
  DocumentRoot /home/webhook-mailer/webhook-mailer/public
  <Directory /home/webhook-mailer/webhook-mailer/public>
     AllowOverride all
     Options -MultiViews
  </Directory>

  ErrorLog ${APACHE_LOG_DIR}/webhook-mailer_error.log
  CustomLog ${APACHE_LOG_DIR}/webhook-mailer_access.log combined

  AllowEncodedSlashes On
  AcceptPathInfo On
</VirtualHost>
```

Enable the module.

```console
$ sudo a2enmod passenger
```

Enable the virtual host.

```console
$ sudo a2ensite webhook-mailer
```

Restart web server.

```console
$ sudo service apache2 restart
```

### Nginx + Unicorn

Prepare following files.

/etc/nginx/sites-enabled/webhook-mailer:
```text
upstream webhook-mailer {
    server unix:/tmp/unicorn-webhook-mailer.sock;
}

server {
    listen 80;
    server_name webhook-mailer.example.com;
    access_log /var/log/nginx/webhook-mailer.example.com-access.log combined;

    root /srv/www/webhook-mailer;
    index index.html;

    proxy_set_header X-Real-IP $remote_addr;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header Host $http_host;
    #proxy_redirect off;

    location / {
        root /home/webhook-mailer/webhook-mailer/public;
        include maintenance;
        if (-f $request_filename){
            break;
        }
        if (!-f $request_filename){
            proxy_pass http://webhook-mailer;
            break;
        }
    }
}
```

/home/webhook-mailer/webhook-mailer/unicorn.conf:
```ruby
# -*- ruby -*-
worker_processes 2
working_directory "/home/webhook-mailer/webhook-mailer"
listen '/tmp/unicorn-github-post-receiver.sock', :backlog => 1
timeout 120
pid 'tmp/pids/unicorn.pid'
preload_app true
stderr_path 'log/unicorn.log'
stdout_path "log/stdout.log"
user "webhook-mailer", "webhook-mailer"
```

/home/webhook-mailer/bin/webhook-mailer:
```zsh
#! /bin/zsh
BASE_DIR=/home/webhook-mailer/webhook-mailer
export RACK_ENV=production
cd  $BASE_DIR
rbenv version

command=$1

function start() {
  mkdir -p $BASE_DIR/tmp/pids
  mkdir -p $BASE_DIR/log
  bundle exec unicorn -D -c unicorn.conf config.ru
}

function stop() {
  kill $(cat $BASE_DIR/tmp/pids/unicorn.pid)
}

function restart() {
  kill -USR2 $(cat $BASE_DIR/tmp/pids/unicorn.pid)
}

$command
```

Install gems.

```console
$ sudo -u webhook-mailer -H bundle install --binstubs --path vendor/bundle
```

Run the application.

```console
$ sudo -u webhook-mailer -H ~webhook-mailer/bin/webhook-mailer start
```

## Configuration

You need to edit `config.yaml` to configure this web application.
See `config.yaml.example` and test codes.
