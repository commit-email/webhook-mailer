# -*- ruby -*-
#
# Copyright (C) 2015  Kenji Okimoto <okimoto@clear-code.com>
# Copyright (C) 2015-2019  Sutou Kouhei <kou@clear-code.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

require_relative "lib/webhook-mailer/version"

Gem::Specification.new do |spec|
  spec.name          = "webhook-mailer"
  spec.version       = WebhookMailer::VERSION
  spec.authors       = ["Kouhei Sutou", "Kenji Okimoto"]
  spec.email         = ["kou@clear-code.com", "okimoto@clear-code.com"]
  spec.summary       = "A Rack application that sends commit details, CI result and so on by E-mail via Webhook."
  spec.description   = "Supported services are GitLab, GitHub, Travis CI and AppVeyor."
  spec.homepage      = "https://gitlab.com/clear-code/webhook-mailer"
  spec.license       = "GPL-3.0+"

  spec.files         = `git ls-files -z`.split("\x0")
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_runtime_dependency "git-commit-mailer"
  spec.add_runtime_dependency "net-smtp"
  spec.add_runtime_dependency "octokit"
  spec.add_runtime_dependency "rack"

  spec.add_development_dependency "bundler"
  spec.add_development_dependency "rake"
  spec.add_development_dependency "test-unit"
  spec.add_development_dependency "test-unit-rr"
  spec.add_development_dependency "test-unit-capybara"
end
